using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PhysicalBullet : Bullet
{
    [SerializeField] private float speed;
    [SerializeField] private float lifetime;

    private new Rigidbody rigidbody;
    private Coroutine     destructionTimerCoroutine;

    private void Awake()
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();
    }

    public override void Shot()
    {
        Vector3 targetPoint;
        
        if (Physics.Raycast(sight.position, sight.forward, out var hit))
            targetPoint = hit.point;
        else
        {
            float distance = speed * lifetime;
            targetPoint = transform.position + transform.forward * distance;
        }
        
        Vector3 direction = (targetPoint - transform.position).normalized;
        rigidbody.AddForce(direction * speed, ForceMode.Impulse);
        
        destructionTimerCoroutine = StartCoroutine(DestructionTimer());
    }

    public override void ChangeActiveState(bool state)
    {
        base.ChangeActiveState(state);
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        StopCoroutine(destructionTimerCoroutine);
        OnHit(collision.gameObject);
    }

    private IEnumerator DestructionTimer()
    {
        yield return new WaitForSeconds(lifetime);
        OnDeactivate?.Invoke();
    }
}
