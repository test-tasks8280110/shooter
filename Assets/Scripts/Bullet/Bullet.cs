using System;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    public Action OnDeactivate;
    
    [SerializeField] private int damage;

    protected Transform sight;
    
    public abstract void Shot();

    public void SetSight(Transform sight) => this.sight = sight;
    
    public virtual void ChangeActiveState(bool state) => gameObject.SetActive(state);

    protected void OnHit(GameObject target)
    {
        target.GetComponent<IDamageable>()?.TakeDamage(damage);
        OnDeactivate?.Invoke();
    }
}
