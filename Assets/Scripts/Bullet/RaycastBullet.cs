using UnityEngine;

public class RaycastBullet : Bullet
{
    public override void Shot()
    {
        if (Physics.Raycast(sight.position, sight.forward, out var hit))
            OnHit(hit.transform.gameObject);
        
        OnDeactivate?.Invoke();
    }
}
