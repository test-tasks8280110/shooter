public interface IInputProvider
{
    public bool GetButtonUp(string buttonName);
    public bool GetButtonDown(string buttonName);
    public bool GetButtonHold(string buttonName);
    public float GetAxis(string axisName);
}
