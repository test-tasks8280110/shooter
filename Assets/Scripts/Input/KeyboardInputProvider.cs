using UnityEngine;

public class KeyboardInputProvider : IInputProvider
{
    public bool GetButtonUp(string buttonName) => Input.GetButtonUp(buttonName);
    
    public bool GetButtonDown(string buttonName) => Input.GetButtonDown(buttonName);
    
    public bool GetButtonHold(string buttonName) => Input.GetButton(buttonName);
    
    public float GetAxis(string axisName) => Input.GetAxis(axisName);
}
