using System;
using System.Collections;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public event Action OnAmmoChanged;

    public int AmountOfAmmunition => amountOfAmmunition;
    public int NumOfRemainingAmmunition
    { 
        get => numOfRemainingAmmunition; 
        set 
        {
            numOfRemainingAmmunition = value;
            OnAmmoChanged?.Invoke();
        } 
    }

    [SerializeField] private int          amountOfAmmunition;
    [SerializeField] private int          reloadTime;
    [SerializeField] private int          costPerShot;
    [SerializeField] private Transform    startPosForAmmo;
    [SerializeField] private GameObject   prefabShooting;

    private ObjectPool<Bullet> objectPool;
    private Coroutine          reloadCoroutine;
    private int                numOfRemainingAmmunition;
    private bool               reloadIsActive;

    private void Awake()
    {
        numOfRemainingAmmunition = amountOfAmmunition;
        objectPool = new ObjectPool<Bullet>(OnCreateNewObjectBullet);
        objectPool.ReturnToPool(OnCreateNewObjectBullet());
    }
    
    public void Shoot()
    {
        if (!reloadIsActive && NumOfRemainingAmmunition >= costPerShot)
        {
            NumOfRemainingAmmunition -= costPerShot;
            var bullet = GetBulletFromPool();
            bullet.ChangeActiveState(true);
            bullet.Shot();
        }
    }

    public void Reload()
    {
        if (NumOfRemainingAmmunition < amountOfAmmunition)
        {
            reloadIsActive = true;
            reloadCoroutine = StartCoroutine(ReloadDelay());
        }
    }

    public void StopReload()
    {
        if (reloadCoroutine != null)
        {
            reloadIsActive = false;
            StopCoroutine(reloadCoroutine);
        }
    }

    public void RefreshAmmunition() => NumOfRemainingAmmunition = amountOfAmmunition;

    public void ChangeActiveState(bool state) => gameObject.SetActive(state);

    private Bullet GetBulletFromPool()
    {
        var bullet = objectPool.GetObject();
        bullet.transform.position = startPosForAmmo.position;
        bullet.transform.rotation = startPosForAmmo.rotation;
        return bullet;
    }

    private Bullet OnCreateNewObjectBullet()
    {
        var bullet = Instantiate(prefabShooting, Vector3.zero, Quaternion.identity).GetComponent<Bullet>();
        bullet.SetSight(transform.parent);
        bullet.OnDeactivate = () =>
        {
            bullet.ChangeActiveState(false);
            objectPool.ReturnToPool(bullet);
        };
        return bullet;
    }


    private IEnumerator ReloadDelay()
    {
        yield return new WaitForSeconds(reloadTime);
        
        reloadIsActive = false;
        RefreshAmmunition();
    }
}
