using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private int        maxNumOfEnemies;
    [SerializeField] private int        spawnInterval;
    [SerializeField] private float      spawnRadius;
    
    private List<Enemy>       activeEnemies;
    private ObjectPool<Enemy> enemyPool;
    private Coroutine         spawnCoroutine;
    private Vector3           centralPosition;
    private int               cntOfActiveEnemies;

    private void Awake()
    {
        enemyPool = new ObjectPool<Enemy>(CreateEnemy);
        activeEnemies = new List<Enemy>(maxNumOfEnemies);
    }
    
    public void SetTargetPosition(Vector3 target) => centralPosition = target;
    
    public void StartGeneratingEnemies() => spawnCoroutine = StartCoroutine(SpawnEnemiesWithInterval());
    
    public void DeactivateAndStopEnemyGeneration()
    {
        StopCoroutine(spawnCoroutine);

        for (int i = 0; i < activeEnemies.Count; i++)
            activeEnemies[i--].OnDeactivate();
    }
    
    private Vector3 CalculateSpawnPosition()
    {
        int angle = UnityEngine.Random.Range(0, 360);
        float x = Mathf.Sin(angle * Mathf.Deg2Rad) * spawnRadius;
        float z = Mathf.Cos(angle * Mathf.Deg2Rad) * spawnRadius;

        return centralPosition + new Vector3(x, 0f, z); 
    }
    
    private Enemy CreateEnemy() => Instantiate(enemyPrefab, Vector3.zero, Quaternion.identity).GetComponent<Enemy>();

    private IEnumerator SpawnEnemiesWithInterval()
    {
        while (true)
        {
            if (activeEnemies.Count < maxNumOfEnemies)
            {
                var enemy = enemyPool.GetObject();
                activeEnemies.Add(enemy);
                
                enemy.ChangeActiveState(true);
                enemy.transform.position = CalculateSpawnPosition();
                enemy.transform.LookAt(centralPosition);
                enemy.OnDeactivate = () =>
                {
                    enemy.ChangeActiveState(false);
                    activeEnemies.Remove(enemy);
                    enemyPool.ReturnToPool(enemy);
                };
            }

            yield return new WaitForSeconds(spawnInterval);
        }
    }
}
