using System;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamageable
{
    public Action OnDeactivate;

    [SerializeField] private int numOfGamePoints;
    [SerializeField] private int speed;
    [SerializeField] private int damage;

    public void ChangeActiveState(bool state) => gameObject.SetActive(state);

    public void TakeDamage(int damage)
    {
        GameManager.Instance.player.GamePoints += numOfGamePoints;
        OnDeactivate?.Invoke();
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            other.gameObject.GetComponent<IDamageable>()?.TakeDamage(damage);
            OnDeactivate?.Invoke();
        }
    }
}
