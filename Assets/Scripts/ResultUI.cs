using System;
using UnityEngine;
using UnityEngine.UI;

public class ResultUI : MonoBehaviour
{
    public event Action OnRestartClick;
    
    [SerializeField] Text   txtScore;
    [SerializeField] Button btnRestart;

    private void Start()
    {
        btnRestart.onClick.AddListener(BtnRestartClick);
    }

    public void Show()
    {
        gameObject.SetActive(true);
        txtScore.text = GameManager.Instance.player.GamePoints.ToString();
    }

    private void BtnRestartClick()
    {
        gameObject.SetActive(false);
        OnRestartClick?.Invoke();
    }
}


