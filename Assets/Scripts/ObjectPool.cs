using System;
using System.Collections.Generic;

public class ObjectPool<T>
{
    private Func<T> CreateNewObjectFunc; 
    
    private Queue<T> _pool;

    public ObjectPool(Func<T> OnCreateNewObject)
    {
        CreateNewObjectFunc = OnCreateNewObject;
        _pool = new Queue<T>();
    }

    public void ReturnToPool(T obj) => _pool.Enqueue(obj);
    
    public T GetObject()
    {
        if (_pool.Count > 0)
            return _pool.Dequeue();

        return CreateNewObjectFunc.Invoke();
    }
}
