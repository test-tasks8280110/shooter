using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private float  rotationSpeed;

    private IInputProvider input;
    
    private const string SHOT = "Fire1";
    private const string TURN_LEFT  = "Horizontal";
    private const string TURN_RIGHT = "Horizontal";
    private const string RELOAD_WEAPON  = "Reload";
    private const string CHANGE_WEAPONS = "CycleWeapon";

    private void Awake()
    {
        input = new KeyboardInputProvider();
    }

    private void Update()
    {
        if (player.IsActive)
        {
            transform.Rotate(Vector3.up, input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime);
            
            if (input.GetButtonDown(SHOT))
                player.Shot();
            else if (input.GetButtonUp(RELOAD_WEAPON))
                player.ReloadWeapon();
            else if (input.GetButtonUp(CHANGE_WEAPONS))
                player.ChangeWeapons();
        }
    }
}
