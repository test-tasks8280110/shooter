using System;
using UnityEngine;

public class Player : MonoBehaviour, IDamageable
{
    public event Action OnDeath;
    public event Action OnWeaponChanged;
    public event Action<int> OnHealthChanged;

    public bool  IsActive { get; private set; }
    public Gun[] Guns => guns;
    public Gun   CurrentWeapon => guns[curActiveGunIndex];
    public int   GamePoints { get; set; }
    public int   Health 
    { 
        get => currentHealth; 
        set 
        {
            currentHealth = value;
            OnHealthChanged?.Invoke(value);
        } 
    }

    [SerializeField] private Gun[] guns;
    [SerializeField] private int   health;
    
    private int currentHealth;
    private int curActiveGunIndex;

    private void Start()
    {
        IsActive = true;
        Health = health;
    }

    public void Reset()
    {
        IsActive = true;
        GamePoints = 0;
        curActiveGunIndex = 0;
        Health = health;
        
        for (var i = Guns.Length - 1; i >= 0; i--)
        {
            Guns[i].ChangeActiveState(false);
            Guns[i].RefreshAmmunition();
        }
        Guns[curActiveGunIndex].ChangeActiveState(true);
    }

    public void TakeDamage(int damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            IsActive = false;
            OnDeath?.Invoke();
        }
        
        OnHealthChanged?.Invoke(Health);
    }

    public void ReloadWeapon() => Guns[curActiveGunIndex].Reload();

    public void ChangeWeapons()
    {
        Guns[curActiveGunIndex].StopReload();
        Guns[curActiveGunIndex].ChangeActiveState(false);
        
        curActiveGunIndex = curActiveGunIndex + 1 == Guns.Length ? 0 : curActiveGunIndex + 1;
        Guns[curActiveGunIndex].ChangeActiveState(true);
        
        OnWeaponChanged?.Invoke();
    }
    
    public void Shot() => Guns[curActiveGunIndex].Shoot();
}
