using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private Text txtAmmoBalance;
    [SerializeField] private Text txtAmmoCapacity;
    [SerializeField] private Text txtHealth;

    private void Start()
    {
        txtHealth.text = GameManager.Instance.player.Health.ToString();

        UpdateAmmoStats();
        Subscribe();
    }

    private void Subscribe()
    {
        GameManager.Instance.player.OnHealthChanged += OnHealthChangedEventHandler;
        
        for (var i = 0; i < GameManager.Instance.player.Guns.Length; i++)
            GameManager.Instance.player.Guns[i].OnAmmoChanged += UpdateAmmoStats;
        GameManager.Instance.player.OnWeaponChanged += UpdateAmmoStats;
    }

    private void OnHealthChangedEventHandler(int health) => txtHealth.text = health.ToString();
    
    private void UpdateAmmoStats()
    {
        txtAmmoCapacity.text = GameManager.Instance.player.CurrentWeapon.AmountOfAmmunition.ToString();
        txtAmmoBalance.text = GameManager.Instance.player.CurrentWeapon.NumOfRemainingAmmunition.ToString();
    }
}
