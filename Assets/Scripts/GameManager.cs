using UnityEngine;

public sealed class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }
    
    [SerializeField] public  Player       player;
    [SerializeField] private EnemySpawner enemySpawner;
    [SerializeField] private ResultUI     resultUI;
    
    private void Awake() 
    {
        if (Instance != null && Instance != this)
            Destroy(this);
        else
            Instance = this;
    }
    
    private void Start()
    {
        Subscribe();
        
        enemySpawner.SetTargetPosition(player.transform.position);
        enemySpawner.StartGeneratingEnemies();
    }

    private void Subscribe()
    {
        player.OnDeath += PlayerKilledEventHandler;
        resultUI.OnRestartClick += RestartClickEventHandler;
    }

    private void PlayerKilledEventHandler()
    {
        enemySpawner.DeactivateAndStopEnemyGeneration();
        resultUI.Show();
    }

    private void RestartClickEventHandler()
    {
        player.Reset();
        enemySpawner.StartGeneratingEnemies();
    }
}
